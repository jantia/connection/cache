<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Cache;

//
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DriverExtendedInterface extends CacheDriverInterface {
	
}
