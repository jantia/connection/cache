<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Cache\Adapter;

//
use Tiat\Collection\Crypt\CryptoBoxInterface;
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CacheDriverExtendedInterface extends CacheDriverInterface {
	
	/**
	 * @return null|CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoInterface() : ?CryptoBoxInterface;
	
	/**
	 * @param    CryptoBoxInterface    $cryptoBox
	 *
	 * @return CacheDriverExtendedInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoInterface(CryptoBoxInterface $cryptoBox) : CacheDriverExtendedInterface;
	
	/**
	 * @param    string|array    $key
	 * @param                    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getItem(string|array $key, $default = NULL) : mixed;
	
	/**
	 * Generates a hash from the given key.
	 *
	 * @param    string|array    $key    The key to generate the hash from.
	 *
	 * @return string The generated hash.
	 * @since   3.0.0 First time introduced.
	 */
	public function generateHash(string|array $key) : string;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getTestSignature() : bool;
	
	/**
	 * @param    bool    $testSignature
	 *
	 * @return CacheDriverExtendedInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTestSignature(bool $testSignature) : CacheDriverExtendedInterface;
}
