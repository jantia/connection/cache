<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Cache\Adapter\Memcached;

//
use Jantia\Connection\Cache\Adapter\CacheDriverExtendedInterface;
use Jantia\Connection\Cache\Exception\InvalidArgumentException;
use Jantia\Connection\Cache\Exception\RuntimeException;
use Tiat\Collection\Crypt\CryptoBoxInterface;
use Tiat\Collection\Crypt\CryptoHelper;
use Tiat\Collection\Crypt\CryptoHelperInterface;
use Tiat\Collection\Crypt\CryptoTrait;
use Tiat\Collection\Crypt\CryptoTraitInterface;
use Tiat\Connection\Cache\Adapter\Memcached\Driver;

use function array_is_list;
use function count;
use function hash;
use function is_array;
use function serialize;
use function unserialize;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DriverExtended extends Driver implements CacheDriverExtendedInterface, CryptoTraitInterface, CryptoHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoHelper;
	
	/**
	 * @var CryptoBoxInterface
	 * @access  private
	 * @desc    The instance of CryptoBoxInterface used for encryption and decryption.
	 * @since   3.0.0 First time introduced.
	 */
	private CryptoBoxInterface $_cryptoBoxInterface;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_allowDecryptedValues = FALSE;
	
	/**
	 * Test signature from encrypted data. Notice! This flag makes system very slow and not recommended if really not needed.
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_testSignature = FALSE;
	
	/**
	 * @param    CryptoBoxInterface    $cryptoBox
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	final public function setCryptoInterface(CryptoBoxInterface $cryptoBox) : static {
		//
		$this->_cryptoBoxInterface = $cryptoBox;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|array    $key
	 * @param                    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	final public function getItem(string|array $key, $default = NULL) : mixed {
		// Get the item from the cache.
		$message = parent::getItem($index = $this->generateHash($key), $default);
		
		// Use openssl to encrypt the value
		if($message !== NULL):
			// Unserialize the value if it's serialized
			if($this->isSerialized($message)):
				$value = unserialize($message, ["allowed_classes" => TRUE]);
			else:
				$value = $message;
			endif;
			
			// Data is always symmetric & openssl encrypted.
			if(is_array($value) && count($value) >= 3 && array_is_list($value) === FALSE):
				//
				$args['iv']  = $value['iv'] ?? NULL;
				$args['tag'] = $value['tag'] ?? NULL;
				$msg         = $value['msg'];
				
				// Decrypt the message
				if(! empty($msg) && ( $secret = $this->getCryptoInterface() ) !== NULL &&
				   $secret instanceof CryptoTraitInterface):
					//
					if(( $crypto = $secret->decrypt($msg, ...$args) ) !== NULL &&
					   $crypto instanceof CryptoBoxInterface):
						//
						$data = $crypto->getCryptoMessage();
						
						// Check if the decrypted data is an array
						if($this->getTestSignature() === TRUE):
							// Test signature from encrypted data
							$secret->createSignature($message);
							if($secret->verifySignature($message) === FALSE):
								throw new InvalidArgumentException("Invalid signature.");
							endif;
						endif;
						
						//
						return $data;
					endif;
				endif;
			endif;
		endif;
		
		// Return the default value if the item wasn't found.
		return $value ?? $default;
	}
	
	/**
	 * @param    string|array    $key
	 *
	 * @return string The generated hash.
	 * @since   3.0.0 First time introduced.
	 */
	final public function generateHash(string|array $key) : string {
		//
		$index = '';
		
		// If array is list then don't use key as the part of the hash.
		if(is_array($key)):
			// Set the flag to false as default.
			$flag = FALSE;
			
			// Check if the array is associative or list.
			if(array_is_list($key)):
				$flag = TRUE;
			endif;
			
			// Sort the array to ensure consistency.
			asort($key, SORT_REGULAR);
			
			// If $key is an array, serialize it first. Then concat each value as to key.
			foreach($key as $k => $v):
				if($flag === TRUE):
					$index .= serialize($v);
				else:
					$index .= serialize([$k => $v]);
				endif;
			endforeach;
		else:
			$index = serialize($key);
		endif;
		
		//
		return hash($this->getHash(), $index, FALSE);
	}
	
	/**
	 * @return null|CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	final public function getCryptoInterface() : ?CryptoBoxInterface {
		return $this->_cryptoBoxInterface ?? NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getTestSignature() : bool {
		return $this->_testSignature;
	}
	
	/**
	 * @param    bool    $testSignature
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setTestSignature(bool $testSignature) : static {
		//
		$this->_testSignature = $testSignature;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|array    $key
	 * @param    mixed           $value
	 * @param    int             $expiration
	 *
	 * @return $this
	 */
	final public function setItem(string|array $key, mixed $value, int $expiration = self::DEFAULT_TTL) : static {
		// Use openssl to encrypt the value
		if(( $secret = $this->getCryptoInterface() ) !== NULL):
			// Check if the value is an array
			$secret->encrypt(serialize($value));
			
			// Create signature from the encrypted data
			if($secret instanceof CryptoBoxInterface):
				// Get the encrypted message
				$data['msg'] = $secret->getCryptoMessage();
				
				// Attach IV and tag to the data
				$data['iv']  = $secret->getOption('iv');
				$data['tag'] = $secret->getOption('tag');
				
				// Set the encrypted value to memcache.
				return parent::setItem($index = $this->generateHash($key), $data, $expiration);
			else:
				$msg = sprintf("Can't encrypt the value because %s is missing.", CryptoBoxInterface::class);
				throw new RuntimeException($msg);
			endif;
		elseif($this->getAllowDecryptedValues() === TRUE):
			// CryptoBoxInterface is missing so set value to memcache if decrypted values are allowed.
			return parent::setItem($index = $this->generateHash($key), $value, $expiration);
		else:
			// Throw an exception because CryptoBoxInterface is missing and decrypted values are not allowed.
			$msg = sprintf("Can't set the value because %s is missing and decrypted values are not allowed.",
			               CryptoBoxInterface::class);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getAllowDecryptedValues() : bool {
		return $this->_allowDecryptedValues;
	}
	
	/**
	 * @param    bool    $allowDecryptedValues
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setAllowDecryptedValues(bool $allowDecryptedValues) : static {
		//
		$this->_allowDecryptedValues = $allowDecryptedValues;
		
		//
		return $this;
	}
	
}
