<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Cache;

//
use Jantia\Connection\Cache\Adapter\CacheDriverExtendedInterface;
use Jantia\Connection\Cache\Adapter\Memcached\DriverExtended;
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;
use Tiat\Connection\Cache\Adapter\Memcached\Connection;
use Tiat\Connection\Cache\Storage;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class StorageExtended extends Storage {
	
	/**
	 * @var CacheDriverExtendedInterface
	 * @since   3.0.0 First time introduced.
	 */
	private CacheDriverExtendedInterface $_driverExtended;
	
	/**
	 * Returns the cache driver instance. This will override the parent method to use the extended driver class.
	 *
	 * @return CacheDriverInterface|CacheDriverExtendedInterface The cache driver instance.
	 * @since   3.0.0 First time introduced.
	 */
	final public function getDriver() : CacheDriverInterface|CacheDriverExtendedInterface {
		//
		if(empty($this->_driverExtended)):
			$this->_createDriver();
		endif;
		
		// Return the (extended) driver instance
		return $this->_driverExtended ?? parent::getDriver();
	}
	
	/**
	 * @param    CacheDriverInterface|NULL    $driver
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _createDriver(CacheDriverInterface $driver = NULL) : void {
		// Use the extended driver class
		if($driver === NULL):
			// Create a Memcached driver extended with connection
			$ext = match ( $this->getAdapter() ) {
				self::ADAPTER_MEMCACHE, self::ADAPTER_MEMCACHED => new DriverExtended(new Connection($this->getSettings())),
				default => NULL
			};
			
			// Set the extended driver instance
			$this->_driverExtended = $ext;
		else:
			// Use the provided driver instance
			parent::_createDriver($driver);
		endif;
	}
}
